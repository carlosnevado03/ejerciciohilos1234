package Controlador;

import Modelo.User;
import Modelo.UserDAO;

import java.sql.*;
import java.util.ArrayList;

public class UserJDBC implements UserDAO {

    public final String SELECT_ALL = "SELECT * FROM Empleados";
    public final String SELECT_BY_ID = "SELECT * FROM Empleados WHERE id = ?";
    public final String SELECT_AVG_SALARY = "SELECT avg(salario) FROM Empleados";
    public final String INSERT_USER = "INSERT INTO Empleados (nombre, apellido, edad, salario, departamento, fecha_contratacion) VALUES (?, ?, ?, ?, ?, ?)";
    public final String DELETE_USER_BY_ID = "DELETE FROM Empleados WHERE id = ?";
    public final String UPDATE_SALARY_BY_ID = "UPDATE Empleados SET salario = ? WHERE id = ?";

    Connection con;

    public UserJDBC(Connection con){
        this.con = con;
    }

    @Override
    public ArrayList<User> select_all() {
        ArrayList<User> lista_user = new ArrayList<>();

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_ALL);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                int edad = rs.getInt("edad");
                double salario = rs.getDouble("salario");
                String departamento = rs.getString("departamento");
                Date fecha_contratacion = rs.getDate("fecha_contratacion");

                User u1 = new User(id, nombre, apellido, edad, salario, departamento, fecha_contratacion);
                lista_user.add(u1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return lista_user;
    }

    @Override
    public User select_by_id(int id) {
        User usuario = null;

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_BY_ID);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                int edad = rs.getInt("edad");
                double salario = rs.getDouble("salario");
                String departamento = rs.getString("departamento");
                Date fecha_contratacion = rs.getDate("fecha_contratacion");

                usuario = new User(id, nombre, apellido, edad, salario, departamento, fecha_contratacion);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return usuario;
    }

    @Override
    public double select_avg_salary() {
        double avgSalary = 0;

        try {
            PreparedStatement stmt = con.prepareStatement(SELECT_AVG_SALARY);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                avgSalary = rs.getDouble(1);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return avgSalary;
    }

    @Override
    public boolean insert_user(User user) {
        try {
            PreparedStatement stmt = con.prepareStatement(INSERT_USER);
            stmt.setString(1, user.getNombre());
            stmt.setString(2, user.getApellido());
            stmt.setInt(3, user.getEdad());
            stmt.setDouble(4, user.getSalario());
            stmt.setString(5, user.getDepartamento());
            stmt.setDate(6, new java.sql.Date(user.getFecha_contratacion().getTime()));
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean delete_user_by_id(int id) {
        try {
            PreparedStatement stmt = con.prepareStatement(DELETE_USER_BY_ID);
            stmt.setInt(1, id);
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean update_salary_by_id(int id, double salary) {
        try {
            PreparedStatement stmt = con.prepareStatement(UPDATE_SALARY_BY_ID);
            stmt.setDouble(1, salary);
            stmt.setInt(2, id);
            int rows = stmt.executeUpdate();
            return rows > 0;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
