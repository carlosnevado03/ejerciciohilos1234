package Modelo;

import java.util.ArrayList;

public interface UserDAO {
    ArrayList<User> select_all();
    User select_by_id(int id);
    double select_avg_salary();
    boolean insert_user(User user);
    boolean delete_user_by_id(int id);
    boolean update_salary_by_id(int id, double salary);
}
