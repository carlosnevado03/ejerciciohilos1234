package Vista;

import Controlador.UserJDBC;
import Modelo.User;
import Utilidades.Conexion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Conexion c = new Conexion();
        Connection conn = null;
        PreparedStatement stmt = null;
        Scanner scanner = new Scanner(System.in);

        try {
            System.out.println("Conectando a la base de datos...");
            conn = c.getConnection();
            UserJDBC controller = new UserJDBC(conn);

            int opcion;
            do {
                System.out.println("\nMenú:");
                System.out.println("1. Mostrar todos los datos de la tabla Empleados");
                System.out.println("2. Mostrar datos de un empleado por ID");
                System.out.println("3. Calcular el salario promedio de los empleados");
                System.out.println("4. Insertar un nuevo empleado");
                System.out.println("5. Eliminar un empleado por ID");
                System.out.println("6. Actualizar el salario de un empleado por ID");
                System.out.println("0. Salir");
                System.out.print("Seleccione una opción: ");
                opcion = scanner.nextInt();

                switch (opcion) {
                    case 1:
                        ArrayList<User> listaEmpleados = controller.select_all();
                        for (User empleado : listaEmpleados) {
                            System.out.println(empleado);
                        }
                        break;
                    case 2:
                        System.out.print("Introduce el ID del empleado: ");
                        int idEmpleado = scanner.nextInt();
                        User empleado = controller.select_by_id(idEmpleado);
                        if (empleado != null) {
                            System.out.println(empleado);
                        } else {
                            System.out.println("No se encontró ningún empleado con ese ID.");
                        }
                        break;
                    case 3:
                        double salarioPromedio = controller.select_avg_salary();
                        System.out.println("El salario promedio de los empleados es: " + salarioPromedio);
                        break;
                    case 4:
                        System.out.println("Insertar nuevo empleado:");
                        // Solicitar datos al usuario y crear un nuevo objeto User
                        // Llamar al método insert_user de controller con el nuevo objeto User
                        break;
                    case 5:
                        System.out.print("Introduce el ID del empleado a eliminar: ");
                        int idEliminar = scanner.nextInt();
                        boolean eliminado = controller.delete_user_by_id(idEliminar);
                        if (eliminado) {
                            System.out.println("Empleado eliminado correctamente.");
                        } else {
                            System.out.println("No se pudo eliminar el empleado.");
                        }
                        break;
                    case 6:
                        System.out.print("Introduce el ID del empleado: ");
                        int idActualizar = scanner.nextInt();
                        System.out.print("Introduce el nuevo salario: ");
                        double nuevoSalario = scanner.nextDouble();
                        boolean actualizado = controller.update_salary_by_id(idActualizar, nuevoSalario);
                        if (actualizado) {
                            System.out.println("Salario actualizado correctamente.");
                        } else {
                            System.out.println("No se pudo actualizar el salario.");
                        }
                        break;
                    case 0:
                        System.out.println("Saliendo del programa...");
                        break;
                    default:
                        System.out.println("Opción no válida. Por favor, seleccione una opción del menú.");
                        break;
                }
            } while (opcion != 0);
        } finally {
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }
        }
    }
}
